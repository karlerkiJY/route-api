const { assert, expect } = require('chai');
const getShortestPath = require('../service/path/shortestPath');

describe('getShortestPath()', () => {
  it('testing simple direct route', () => {
    let graph = {
      A: {
        B: 1,
        C: 1,
      },
      B: {
        C: 1,
      },
      C: {
        B: 1,
      },
    };
    let startNode = 'A';
    let endNode = 'C';
    var allowedStepsN = 4;

    let result = getShortestPath(graph, startNode, endNode, allowedStepsN);

    expect(result.distance).to.equal(1);
    expect(result.path).to.deep.equal(['A', 'C']);
  });

  it('testing longer route', () => {
    let graph = {
      A: {
        B: 1,
        D: 100,
      },
      B: {
        C: 1,
      },
      C: {
        D: 1,
      },
      D: {
        A: 1,
      },
    };
    let startNode = 'A';
    let endNode = 'D';
    var allowedStepsN = 4;

    let result = getShortestPath(graph, startNode, endNode, allowedStepsN);

    expect(result.distance).to.equal(3);
    expect(result.path).to.deep.equal(['A', 'B', 'C', 'D']);
  });

  it('testing step limit', () => {
    let graph = {
      A: {
        B: 1,
        C: 50,
        D: 100,
      },
      B: {
        C: 1,
        D: 50,
      },
      C: {
        D: 20,
      },
      D: {
        A: 1,
      },
    };
    let startNode = 'A';
    let endNode = 'D';
    var allowedStepsN = 2;

    let result = getShortestPath(graph, startNode, endNode, allowedStepsN);

    expect(result.distance).to.equal(51);
    expect(result.path).to.deep.equal(['A', 'B', 'D']);
  });

  it('testing step limit', () => {
    let graph = {
      A: {
        B: 1,
        C: 50,
        D: 100,
      },
      B: {
        C: 1,
        D: 50,
      },
      C: {
        D: 20,
      },
      D: {
        A: 1,
      },
    };
    let startNode = 'A';
    let endNode = 'D';
    var allowedStepsN = 1;

    let result = getShortestPath(graph, startNode, endNode, allowedStepsN);

    expect(result.distance).to.equal(100);
    expect(result.path).to.deep.equal(['A', 'D']);
  });

  it('testing a non-viable route', () => {
    let graph = {
      A: {
        B: 1,
        C: 50,
      },
      B: {
        C: 1,
        D: 50,
      },
      C: {
        D: 20,
      },
      D: {
        A: 1,
      },
    };
    let startNode = 'A';
    let endNode = 'D';
    var allowedStepsN = 1;

    assert.throws(() => getShortestPath(graph, startNode, endNode, allowedStepsN), Error, 'ROUTE_NOT_POSSIBLE');
  });
});
