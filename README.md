# API description

The route api is available on port 8081. GET request. Required query parameters:

- source - IATA or ICAO code of the source airport
- destination - IATA or ICAO code of the destination airport

Returns distance in km-s and the path shown in IATA codes

Example: {"distance":345.156,"path":["TLL","HEL","TAY"]}

## Data

Data included in the project is from https://openflights.org/data.html

**Note that route data is historical.**

## Data import

To import data replace the routes and/or airports files.

### Airports file must contain the following data:

- IATA - IATA code of the airport
- ICAO - ICAO code of the airport
- lat - latitude of the airport
- lng - longitude of the airport

### Routes file must contain the following data:

- sourceAirport - IATA code of the source airport
- destinationAirport - IATA code of the destination airport
