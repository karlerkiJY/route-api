const csv = require('csvtojson');
const geolib = require('geolib');

let airports = {};
let routeGraphs = {};

module.exports.getAirportsAndRoutes = async () => {
  let airports = await getAirports();

  if (Object.keys(airports).length < 1) throw new Error('AIRPORTS_FILE_ISSUE');

  let routeGraphs = await getRouteGraphs(airports);

  if (Object.keys(airports).length < 1) throw new Error('ROUTES_FILE_ISSUE');

  return { airports, routeGraphs };
};

const getAirports = async () => {
  if (Object.keys(airports).length > 0) return airports;

  let rawAirports = await csv({ checkColumn: true }).fromFile('airports.csv');

  airports = rawAirports.reduce((map, obj) => {
    map[obj.IATA] = obj;
    map[obj.ICAO] = obj;
    return map;
  }, {});

  return airports;
};

const getRouteGraphs = async (airports) => {
  if (Object.keys(routeGraphs).length > 0) return routeGraphs;

  routes = await csv().fromFile('routes.csv');

  let routesWithDistnaces = getRoutesWithDistances(routes, airports);

  for (let route of routesWithDistnaces) {
    if (routeGraphs[route.sourceAirport]) {
      routeGraphs[route.sourceAirport][route.destinationAirport] = route.distance;
    } else {
      let obj = {};
      obj[route.destinationAirport] = route.distance;
      routeGraphs[route.sourceAirport] = obj;
    }
  }

  return routeGraphs;
};

const getRoutesWithDistances = (routes, airports) => {
  let routesWithDistnaces = [];

  for (let route of routes) {
    let sourceAirport = airports[route.sourceAirport];
    let destinationAirport = airports[route.destinationAirport];

    if (!sourceAirport || !destinationAirport) continue;

    route.distance =
      geolib.getDistance(
        { latitude: sourceAirport.lat, longitude: sourceAirport.lng },
        { latitude: destinationAirport.lat, longitude: destinationAirport.lng },
      ) / 1000;

    routesWithDistnaces.push(route);
  }

  return routesWithDistnaces;
};
