const { performance } = require('perf_hooks');

let getShortestPath = require('./shortestPath');

module.exports.getShortestRoute = (dataObj, source, destination) => {
  let sourceAirport = dataObj.airports[source];
  let destinationAirport = dataObj.airports[destination];

  if (!sourceAirport || !sourceAirport.IATA) throw new Error('INVALID_SOURCE_AIRPORT');
  if (!destinationAirport || !destinationAirport.IATA) throw new Error('INVALID_DESTINATION_AIRPORT');

  let startTime = performance.now();
  let result = getShortestPath(dataObj.routeGraphs, sourceAirport.IATA, destinationAirport.IATA, 4);
  let endTime = performance.now();

  console.debug('Elapsed time:', (endTime - startTime) / 1000 + ' seconds');

  return result;
};
