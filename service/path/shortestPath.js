/* 
    Code from https://github.com/noamsauerutley/shortest-path
    Optimized and modified for our usecase.
*/

const shortestDistanceNode = (distances, visited) => {
  let shortest = null;

  for (let node in distances) {
    let currentIsShortest = shortest === null || distances[node] < distances[shortest];
    if (currentIsShortest && !visited.includes(node)) {
      shortest = node;
    }
  }
  return shortest;
};

const getPathLen = (node, parents) => {
  let pathLen = 0;
  let parent = parents[node];

  while (parent) {
    pathLen++;
    parent = parents[parent];
  }

  return pathLen;
};

const getShortestPath = (graph, startNode, endNode, allowedStepsN) => {
  // establish object for recording distances from the start node
  let distances = {};
  distances[endNode] = Infinity;
  distances = Object.assign(distances, graph[startNode]);

  if (!graph[startNode]) throw new Error('ROUTE_NOT_POSSIBLE');

  // track paths
  let parents = { endNode: null };
  for (let child in graph[startNode]) {
    parents[child] = startNode;
  }

  // track nodes that have already been visited
  let visited = [];

  // find the nearest node
  let node = shortestDistanceNode(distances, visited);

  // for that node
  while (node) {
    // find its distance from the start node & its child nodes
    let distance = distances[node];

    if (node === endNode || distances[endNode] <= distance) {
      visited.push(node);
      node = shortestDistanceNode(distances, visited);

      continue;
    }

    if (allowedStepsN > 0 && getPathLen(node, parents) >= allowedStepsN) {
      visited.push(node);
      node = shortestDistanceNode(distances, visited);

      continue;
    }

    let children = graph[node];

    // for each of those child nodes
    for (let child in children) {
      // make sure each child node is not the start node
      if (String(child) === String(startNode)) continue;

      // save the distance from the start node to the child node
      let newdistance = distance + children[child];

      if (newdistance > distances[endNode]) continue;

      // if there's no recorded distance from the start node to the child node in the distances object
      // or if the recorded distance is shorter than the previously stored distance from the start node to the child node
      // save the distance to the object
      // record the path
      if (!distances[child] || distances[child] > newdistance) {
        distances[child] = newdistance;
        parents[child] = node;
      }
    }

    // move the node to the visited set
    visited.push(node);
    // move to the nearest neighbor node
    node = shortestDistanceNode(distances, visited);
  }

  // using the stored paths from start node to end node
  // record the shortest path
  let shortestPath = [endNode];
  let parent = parents[endNode];
  while (parent) {
    shortestPath.unshift(parent);
    parent = parents[parent];
  }

  if (distances[endNode] === Infinity) throw new Error('ROUTE_NOT_POSSIBLE');

  // return the shortest path from start node to end node & its distance
  return {
    distance: distances[endNode],
    path: shortestPath,
  };
};

module.exports = getShortestPath;
