const express = require('express');
const { query, validationResult } = require('express-validator');
const app = express();

const dataService = require('./service/data/index');
const pathService = require('./service/path/index');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

module.exports = app;

const port = process.env.PORT || 8081;

var server = app.listen(port, () => {
  var port = server.address().port;
  console.log('App listening on port', port);
});

app.get(
  '/',
  query('source').isLength({ min: 3, max: 4 }).trim().toUpperCase(),
  query('destination').isLength({ min: 3, max: 4 }).trim().toUpperCase(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    let dataObj;
    try {
      dataObj = await dataService.getAirportsAndRoutes();
    } catch (err) {
      return res.status(500).json(err.toString());
    }

    let source = req.query.source;
    let destination = req.query.destination;

    let result;
    try {
      result = pathService.getShortestRoute(dataObj, source, destination);
    } catch (err) {
      return res.status(400).json(err.toString());
    }

    res.json(result);
  },
);
